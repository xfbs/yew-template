# Yew Template

This repository contains a template for an empty Yew project. It uses
[trunk](https://trunkrs.dev/) to help generate a website from it.

The template has [tailwind](https://tailwindcss.com/) setup, so that you
can use any of the CSS classes.

## Setup

You need to install toolchain support for the WebAssembly target.

    rustup target add wasm32-unknown-unknown

You need to install the [trunk](https://trunkrs.dev/) tool, which helps you
turn the WebAssembly blobs into working websites.

    cargo install trunk

## Usage

You can build the website using `trunk`.

    trunk build

You can also tell trunk to start a server and recompile it when any of the
sources change. This is useful for development and quick feedback loops.

    trunk serve

## License

MIT.
