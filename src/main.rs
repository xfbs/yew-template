use yew::prelude::*;

#[function_component]
fn App() -> Html {
    let counter = use_state(|| 0);

    let onclick = {
        let counter = counter.clone();
        move |_| {
            counter.set(*counter + 1);
        }
    };

    html! {
        <div>
            <h1 class="text-xl font-bold">{"Hello, World"}</h1>
            <p>{"Counter: "}{counter.to_string()}</p>
            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" {onclick}>{"Increment"}</button>
        </div>
    }
}

fn main() {
    wasm_logger::init(Default::default());
    yew::Renderer::<App>::new().render();
}
